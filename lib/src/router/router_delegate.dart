import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'router_manager.dart';
import 'router_config.dart';

class AppRouterDelegate extends RouterDelegate<AppRouterConfig>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRouterConfig> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  final _routerManager = AppRouterManager();

  AppRouterDelegate() {
    _routerManager.addListener(notifyListeners);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _routerManager,
      child: Consumer<AppRouterManager>(
        builder: (context, value, child) {
          return Navigator(
            pages: _routerManager.pages,
            onPopPage: (route, result) {
              print("Llamada a onPopPage: ${route.settings.name}");
              bool didPop = route.didPop(result);

              if (!didPop) {
                return false;
              }

              return _routerManager.didPop(route.settings);
            },
          );
        },
      ),
    );
  }

  @override
  GlobalKey<NavigatorState>? get navigatorKey => _navigatorKey;

  @override
  AppRouterConfig? get currentConfiguration {
    AppRouterConfig configuration =
        AppRouterManager.getRouterConfig(_routerManager.pages.last.name ?? '');
    print("Llamada a currentConfiguration: ${configuration.config}");
    return configuration;
  }

  @override
  Future<void> setNewRoutePath(AppRouterConfig configuration) async {
    print("Llamada a setNewRoutePath: ${configuration.config}");
    switch (configuration.config) {
      case AppRouterConfigState.home:
        _routerManager.goHome();
        break;
      case AppRouterConfigState.item:
        _routerManager.openItemPage(configuration.itemNum);
        break;
      default:
        _routerManager.viewUnknown();
    }
  }
}
