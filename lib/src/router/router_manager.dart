import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../router/router_config.dart';
import '../pages/home.dart';
import '../pages/unknown.dart';
import '../pages/item1.dart';
import '../pages/item2.dart';
import '../pages/item3.dart';
import '../pages/item4.dart';

class AppRouterManager extends ChangeNotifier {
  // Método para recuperar la instancia de AppRouterManager del contexto
  static AppRouterManager of(BuildContext context) {
    return Provider.of<AppRouterManager>(context, listen: false);
  }

  // Lista de paginas del router
  final List<Page> _pages = [createHomePage()];

  List<Page> get pages => List.unmodifiable(_pages);

  // Método para obtener la configuración a partir de una ruta
  static AppRouterConfig getRouterConfig(String location) {
    final uri = Uri.parse(location);

    if (uri.pathSegments.isEmpty) {
      return AppRouterConfig.home();
    } else if (uri.pathSegments.length == 2 &&
        uri.pathSegments.first == 'item') {
      return AppRouterConfig.item(int.tryParse(uri.pathSegments.last));
    }
    return AppRouterConfig.unknown();
  }

  // Método para obtener la ruta a partir de una configuración
  static String getLocation(AppRouterConfig configState) {
    switch (configState.config) {
      case AppRouterConfigState.home:
        return '/';
      case AppRouterConfigState.item:
        return '/item/${configState.itemNum}';
      default:
        return '/404';
    }
  }

  // Método para crear una HomePage
  static MaterialPage createHomePage() {
    return MaterialPage(
      key: UniqueKey(),
      name: getLocation(AppRouterConfig.home()),
      child: const HomePage(),
    );
  }

  // Método para crear una UnknownPage
  MaterialPage createUnknownPage() {
    return MaterialPage(
      key: UniqueKey(),
      name: getLocation(AppRouterConfig.unknown()),
      child: const UnknownPage(),
    );
  }

  // Método para crear un ItemPage
  MaterialPage createItemPage(int? numero) {
    String name = getLocation(AppRouterConfig.item(numero));

    switch (numero) {
      case 1:
        return MaterialPage(
          key: UniqueKey(),
          name: name,
          child: const Item1Page(),
        );
      case 2:
        return MaterialPage(
          key: UniqueKey(),
          name: name,
          child: const Item2Page(),
        );
      case 3:
        return MaterialPage(
          key: UniqueKey(),
          name: name,
          child: const Item3Page(),
        );
      case 4:
        return MaterialPage(
          key: UniqueKey(),
          name: name,
          child: const Item4Page(),
        );
      default:
        return createUnknownPage();
    }
  }

  // Método para popear una página de la lista
  bool didPop(RouteSettings settings) {
    bool result = _pages.remove(settings);
    notifyListeners();
    return result;
  }

  // Método para ir al HomePage
  void goHome() {
    for (int i = _pages.length - 1; i > 0; i--) {
      _pages.removeAt(i);
    }
    notifyListeners();
  }

  // Método para mostrar la página desconocida
  void viewUnknown() {
    if (_pages.last.name == getLocation(AppRouterConfig.unknown())) return;
    _pages.add(createUnknownPage());
    notifyListeners();
  }

  // Método para abrir un item
  void openItemPage(int? numero) {
    MaterialPage page = createItemPage(numero);
    if (page.name == _pages.last.name) return;
    if (_pages.last.name == getLocation(AppRouterConfig.unknown())) {
      _pages.remove(_pages.last);
    }
    _pages.add(page);
    notifyListeners();
  }
}
