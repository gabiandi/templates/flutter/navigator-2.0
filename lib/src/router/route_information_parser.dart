import 'package:flutter/material.dart';

import 'router_manager.dart';
import 'router_config.dart';

class AppRouteInformationParser
    extends RouteInformationParser<AppRouterConfig> {
  @override
  Future<AppRouterConfig> parseRouteInformation(
      RouteInformation routeInformation) async {
    print("Llamada a parseRouteInformation: ${routeInformation.location}");
    return AppRouterManager.getRouterConfig(routeInformation.location ?? '/');
  }

  @override
  RouteInformation? restoreRouteInformation(AppRouterConfig configuration) {
    String location = AppRouterManager.getLocation(configuration);
    print("Llamada a restoreRouteInformation: $location");
    return RouteInformation(location: location);
  }
}
