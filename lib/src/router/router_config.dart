enum AppRouterConfigState { home, unknown, item }

class AppRouterConfig {
  final AppRouterConfigState _config;

  int? itemNum;

  AppRouterConfigState get config => _config;

  AppRouterConfig.home() : _config = AppRouterConfigState.home;
  AppRouterConfig.unknown() : _config = AppRouterConfigState.unknown;
  AppRouterConfig.item(this.itemNum) : _config = AppRouterConfigState.item;
}
