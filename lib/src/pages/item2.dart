import 'package:flutter/material.dart';

class Item2Page extends StatelessWidget {
  const Item2Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Item 2"),
      ),
      body: const Center(
        child: Text("Página del item 2"),
      ),
    );
  }
}
