import 'package:flutter/material.dart';

import '../router/router_manager.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Navigator 2.0 example"),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        padding: const EdgeInsets.all(20),
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
        children: [
          Container(
            color: Colors.green,
            child: MaterialButton(
              child: const Text(
                "Item 1",
                style: TextStyle(fontSize: 34),
              ),
              onPressed: () => AppRouterManager.of(context).openItemPage(1),
            ),
          ),
          Container(
            color: Colors.orange,
            child: MaterialButton(
              child: const Text(
                "Item 2",
                style: TextStyle(fontSize: 34),
              ),
              onPressed: () => AppRouterManager.of(context).openItemPage(2),
            ),
          ),
          Container(
            color: Colors.yellow,
            child: MaterialButton(
              child: const Text(
                "Item 3",
                style: TextStyle(fontSize: 34),
              ),
              onPressed: () => AppRouterManager.of(context).openItemPage(3),
            ),
          ),
          Container(
            color: Colors.purple,
            child: MaterialButton(
              child: const Text(
                "Item 4",
                style: TextStyle(fontSize: 34),
              ),
              onPressed: () => AppRouterManager.of(context).openItemPage(4),
            ),
          )
        ],
      ),
    );
  }
}
