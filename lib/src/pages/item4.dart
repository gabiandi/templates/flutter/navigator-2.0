import 'package:flutter/material.dart';

class Item4Page extends StatelessWidget {
  const Item4Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Item 4"),
      ),
      body: const Center(
        child: Text("Página del item 4"),
      ),
    );
  }
}
