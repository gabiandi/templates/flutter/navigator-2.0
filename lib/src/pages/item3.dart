import 'package:flutter/material.dart';

class Item3Page extends StatelessWidget {
  const Item3Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Item 3"),
      ),
      body: const Center(
        child: Text("Página del item 3"),
      ),
    );
  }
}
