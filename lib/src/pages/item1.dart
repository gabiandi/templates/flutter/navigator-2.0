import 'package:flutter/material.dart';

class Item1Page extends StatelessWidget {
  const Item1Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Item 1"),
      ),
      body: const Center(
        child: Text("Página del item 1"),
      ),
    );
  }
}
