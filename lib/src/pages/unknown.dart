import 'package:flutter/material.dart';

class UnknownPage extends StatelessWidget {
  const UnknownPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Página no encontrada"),
      ),
      body: const Center(
        child: Text("La página a la que quiere acceder no existe"),
      ),
    );
  }
}
