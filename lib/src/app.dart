import 'package:flutter/material.dart';

import 'router/router_delegate.dart';
import 'router/route_information_parser.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: "Navigator 2.0 example",
      routerDelegate: AppRouterDelegate(),
      routeInformationParser: AppRouteInformationParser(),
    );
  }
}
