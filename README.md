# Ejemplo de uso de Navigator 2.0

Este repositorio busca mostrar con ejemplo de código lo mas simple posible, como funciona el **Navigator 2.0** de **Flutter**. Este manera de definir rutas y páginas, es especialmente útil cuando tenemos una implementación para el navegador.

![nav20](doc/nav20.png)

## Descripción

La idea es que usted pueda ver el código fuente y entender a base de debugs y prints el recorrido del Navigator, por lo demás, las rutas y el árbol de navegación es bastante sencillo. Tenemos el ***HomePage***, ***ItemPage*** que podemos acceder, y en caso de ingresar a una ruta desconocida un ***UnknownPage*** equivalente a un ***404***.

Este proyecto no busca explicar en detalle como funciona el Navigator, para ello ya existe mucha información en páginas externas.

## Páginas útiles

- [Artículo de Medium.](https://medium.com/comunidad-flutter/aprendiendo-del-nuevo-navigator-2-0-y-del-sistema-de-routing-450a1d60e210)
- [Vídeos de Ricardo Markiewicz.](https://www.youtube.com/@AndroideDelValle)
- [Vídeos de discusión.](https://www.youtube.com/watch?v=u5bcmaS-JtE)
- [Vídeos de Fernando Herrera.](https://www.youtube.com/@DevTalles)
